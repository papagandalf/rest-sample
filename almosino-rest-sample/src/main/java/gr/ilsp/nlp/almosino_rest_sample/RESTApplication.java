package gr.ilsp.nlp.almosino_rest_sample;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("resources")
public class RESTApplication extends ResourceConfig {
    public RESTApplication() {
        packages("gr.ilsp.nlp.almosino_rest_sample");
    }
}