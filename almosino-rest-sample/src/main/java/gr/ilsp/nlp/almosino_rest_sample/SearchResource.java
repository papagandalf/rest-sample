package gr.ilsp.nlp.almosino_rest_sample;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;


import javax.ws.rs.NotFoundException;


/**
 * @author papagandalf(npapasa@ilsp.gr)
 */

@Singleton
@Path("")
//@Api(value = "job management", description = "preprocessing job management and monitoring services")
//@CrossOriginResourceSharing(allowAllOrigins = true)
public class SearchResource {
	private static final Logger logger = LoggerFactory.getLogger(SearchResource.class.getCanonicalName());
	public SearchResource(){
		logger.info("SearchResource is called.......");
	}
	
    
	/**
	 * return the status of the job with specific id
	 * @param id
	 * @return
	 */
	@GET
	@Path("/stuff")
//	@ApiOperation("return the status of the job with specific id")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response checkStatusInDetail(/*@ApiParam("asd")*/ @QueryParam("id") final String id)
	{
		logger.info("[querying status of job] " + id);

		try{
			JsonObject response = new JsonObject();
			response.addProperty("status", "a");
			response.addProperty("copied", 2);
			response.addProperty("total", 4);
			response.addProperty("percentage", 0.5);
			Response res = Response.status(Status.OK).entity(response.toString()).header("Access-Control-Allow-Origin", "*").type(MediaType.APPLICATION_JSON).build();
			return res;
		}
		catch(Exception e){e.printStackTrace();throw new NotFoundException("Error 500 (Internal Server Error) \nThere was a problem with JSON.");}	
	}
}